import pygame, sys, time

pygame.init()
pygame.mixer.init()

class App:
    def __init__(self):
        #CONSTANTES
        self.SCREEN = 1000
        self.IMG_SIZE = 2000
        self.velocity = 0.5
        self.maxVelocity = 7
        self.IMG = pygame.image.load("IMG/poing3.png")
        self.SONGS = {"OPM":'SOUNDS/OPM.mp3', "SnK":'SOUNDS/SnK.mp3', "Sakamoto":'SOUNDS/Sakamoto.mp3'} #banque de musique

        #init
        self.root = pygame.display.set_mode((self.SCREEN,self.SCREEN))
        self.pos = (self.SCREEN - self.IMG_SIZE)/2
        self.vel = 0
        self.lock_j1 = [0, 0]
        self.lock_j2 = [0, 0]
        self.music = pygame.mixer.music.load(self.SONGS["OPM"]) #musique par défault
        self.start() # Start animation
        self.loopEvents()

    def draw(self):
        pygame.draw.rect(self.root, (0,0,0), (self.pos, self.pos, self.IMG_SIZE, self.IMG_SIZE))
        self.keysEvents()
        self.root.blit(self.IMG, (self.pos, self.pos))


    def keysEvents(self):
        self.keys = pygame.key.get_pressed()
        if self.pos >= -self.IMG_SIZE/2 and self.pos <= self.SCREEN - self.IMG_SIZE/2:
            self.pos += self.vel
        else:
            if self.pos >= -self.IMG_SIZE/2:
                print("J1 win")
                pygame.quit()
                sys.exit()
            if self.pos <= self.SCREEN - self.IMG_SIZE/2:
                print("J2 win")
                pygame.quit()
                sys.exit()

        #binds j1
        if not self.keys[pygame.K_KP1] and self.vel <= self.maxVelocity:
            self.lock_j1[0] = 0

        if not self.keys[pygame.K_KP2] and self.vel <= self.maxVelocity:
            self.lock_j1[1] = 0

        if self.keys[pygame.K_KP1] and self.lock_j1[0] == 0:
            self.vel -= self.velocity
            self.lock_j1[0] = 1

        if self.keys[pygame.K_KP2] and self.lock_j1[1] == 0:
            self.vel -= self.velocity
            self.lock_j1[1] = 1

        #binds j2
        if not self.keys[pygame.K_1] and self.vel <= self.maxVelocity:
            self.lock_j2[0] = 0

        if not self.keys[pygame.K_2] and self.vel <= self.maxVelocity:
            self.lock_j2[1] = 0

        if self.keys[pygame.K_1] and self.lock_j2[0] == 0:
            self.vel += self.velocity
            self.lock_j2[0] = 1

        if self.keys[pygame.K_2] and self.lock_j2[1] == 0:
            self.vel += self.velocity
            self.lock_j2[1] = 1

        #SELECTION DES MUSIQUES

        if self.keys[pygame.K_F1]:
            pygame.mixer.music.stop()
            pygame.mixer.music.load(self.SONGS["OPM"])
            pygame.mixer.music.play(0)

        if self.keys[pygame.K_F2]:
            pygame.mixer.music.stop()
            pygame.mixer.music.load(self.SONGS["SnK"])
            pygame.mixer.music.play(0)

        if self.keys[pygame.K_F3]:
            pygame.mixer.music.stop()
            pygame.mixer.music.load(self.SONGS["Sakamoto"])
            pygame.mixer.music.play(0)

        if self.keys[pygame.K_F4]:
            pygame.mixer.music.stop()

    # Mainloop
    def loopEvents(self):
        self.FPS = pygame.time.Clock()
        pygame.mixer.music.play(0)
        while True:
            self.FPS.tick(60)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
            self.draw()
            pygame.display.update()

    def start(self):
        self.IMG_START_LISTE = ["IMG/3.png", "IMG/2.png", "IMG/1.png", "IMG/FIGHT.png"]
        for img in range(len(self.IMG_START_LISTE)):
            pygame.draw.rect(self.root, (0,0,0), (self.pos, self.pos, self.IMG_SIZE, self.IMG_SIZE))
            self.img_start = pygame.image.load(self.IMG_START_LISTE[img])
            self.root.blit(self.img_start, (self.pos, self.pos))
            pygame.display.update()
            time.sleep(1)

a = App()
